import React, { Component } from "react";
import GlassItem from "./GlassItem";

export default class ListGlass extends Component {
  renderListGlass = () => {
    return this.props.glassArr.map((item) => {
      return (
        <GlassItem
          handleChangeDetail={this.props.handleChangeDetail}
          data={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListGlass()}</div>;
  }
}
